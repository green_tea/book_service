<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// authors
Route::post('/authors/create', 'AuthorController@create');
Route::get('/authors', 'AuthorController@all');
Route::get('/authors/{author}', 'AuthorController@get');


// books
Route::post('/books/create/with_author', 'BookController@createWithAuthor');
Route::get('/books/count', 'BookController@count');
Route::get('/books', 'BookController@all');
Route::get('/books/{book}', 'BookController@get');
Route::post('/books/set/in_plan/{book}', 'BookController@setInPlan');
Route::post('/books/set/has_read/{book}', 'BookController@setHasRead');