const bookAdapter = (book) => {
    return {
        id: book.id,
        authors: book.authors,
        name: book.name,
        inPlan: book.in_plan,
        hasRead: book.has_read,
    }
};

export default bookAdapter;