import Vuex from 'vuex';

const modules = {

};

const state = {
    csrfToken: '',
};

const getters = {
    csrf: ({ csrfToken }) => csrfToken,
};

const actions = {
    nuxtServerInit({ commit }) {
    },
    nuxtClientInit({ dispatch }) {
    },
};

const mutations = {
};

const createStore = () => new Vuex.Store({
    modules,
    state,
    getters,
    actions,
    mutations,
});

export default createStore;
