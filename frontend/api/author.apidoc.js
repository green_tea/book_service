/**
 * @api {post} /api/authors/create
 * @apiName createAuthor
 * @apiGroup authors
 * @apiDescription create new author
 */
/**
 * @api {get} /api/authors
 * @apiName authors
 * @apiGroup authors
 * @apiDescription get all authors with pagination
 */
/**
 * @api {get} /api/authors/:author
 * @apiName author
 * @apiGroup authors
 * @apiDescription get author by id
 */