/**
 * @api {post} /api/books/create/with_author
 * @apiName saveBooks
 * @apiGroup books
 * @apiDescription save all books by strings: 228) Ванька Пупкин «Супер книга»
 */
/**
 * @api {get} /api/books/count
 * @apiName countBooks
 * @apiGroup books
 * @apiDescription count books number
 */
/**
 * @api {get} /api/books
 * @apiName allBooks
 * @apiGroup books
 * @apiDescription get all books with pagination
 */
/**
 * @api {get} /api/books/:book
 * @apiName book
 * @apiGroup books
 * @apiDescription get book by id
 */
/**
 * @api {post} /api/books/set/in_plan/:book
 * @apiName setBookInPlan
 * @apiGroup books
 * @apiDescription set in plan for book
 */
/**
 * @api {post} /api/books/set/has_read/:book
 * @apiName setBookHasRead
 * @apiGroup books
 * @apiDescription set has read for book
 */