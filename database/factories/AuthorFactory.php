<?php

use Faker\Generator as Faker;
use App\Models\Author;


$factory->define(Author::class, function (Faker $faker) {
    return [
        'full_name' => $faker->name,
        'first_name' => $faker->firstName,
        'middle_name' => $faker->firstName,
        'last_name' => $faker->lastName,
    ];
});
