<?php

use App\Models\AuthorBook;
use App\Models\Book;
use Faker\Generator as Faker;

$factory->define(AuthorBook::class, function (Faker $faker) {
    return [
        'book_id' => function() {
            factory(Book::class)->create()->id;
        },
        'author_id' => function() {
            factory(Author::class)->create()->id;
        }
    ];
});
