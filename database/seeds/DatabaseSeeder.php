<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    private function createAdmin($name, $password, $email) {

        if (User::where('email', $email)->first()) return;

        factory(User::class)->create([
            'password' => Hash::make($password),
            'email' => $email,
        ]);
    }

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->createAdmin('Ветлин Владислав Сергеевич', 'secret', 'ice_tea@mail.ru');


        $this->call(UsersSeeder::class);
        $this->call(FacultiesSeeder::class);
        $this->call(ArticlesSeeder::class);
    }
}
