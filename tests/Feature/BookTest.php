<?php

namespace Tests\Feature;

use App\Models\Author;
use App\Models\AuthorBook;
use App\Models\Book;
use App\Test;
use Tests\TestCase;

class BookTest extends TestCase
{

    public function testAll() {
        $books = factory(Book::class, 3)->create();

        $data = [
            'per_page' => 1,
            'cur_page' => 2,
        ];

        $response = $this->getQ('/api/books', $data)->json()['result'];

        self::assertEquals(3, $response['total']);
        self::assertEquals($books[1]->id, $response['data'][0]['id']);
    }

    public function testGet() {
        $book = factory(Book::class)->create();

        $response = $this->get("/api/books/{$book->id}")->json()['result'];

        self::assertEquals($book->id, $response['id']);
    }

    public function testCreateWithAuthor1() {
        $data = [
            'data' => ["Альфред Позаментьев «Стратегии решения математических задач»"],
        ];

        $this->post('/api/books/create/with_author', $data)->json();

        $book = Book::first();
        $author = Author::first();

        self::assertEquals('Стратегии решения математических задач', $book->name);
        self::assertEquals('Альфред Позаментьев', $author->full_name);
    }

    public function testCreateWithAuthor2() {
        $data = [
            'data' => ["1357) Аркадий Стругацкий, Борис Стругацкий. «Полдень, XXII век»"],
        ];

        $this->post('/api/books/create/with_author', $data);

        $book = Book::first();
        $author = Author::first();

        self::assertEquals('Полдень, XXII век', $book->name);
        self::assertEquals('Аркадий Стругацкий, Борис Стругацкий.', $author->full_name);
    }

    public function testCreateWithAuthor3() {
        $data = [
            'data' => ["1174) Мигель Гринберг «Flask Web Development»"],
        ];

        $this->post('/api/books/create/with_author', $data);

        $book = Book::first();
        $author = Author::first();

        self::assertEquals('Flask Web Development', $book->name);
        self::assertEquals('Мигель Гринберг', $author->full_name);
    }

    public function testCreateWithAuthorWithMany() {
        $data = [
            'data' => [
                "1174) Мигель Гринберг «Flask Web Development»",
                "1174) Мигель Гринберг «Flask Web Development»",
                "1357) Аркадий Стругацкий, Борис Стругацкий. «Полдень, XXII век»",
                "Альфред Позаментьев «Стратегии решения математических задач»",
            ],
        ];

        $this->post('/api/books/create/with_author', $data);

        self::assertCount(3, Book::all());
        self::assertCount(3, Author::all());
    }

    public function testCreateWithAuthorWithManySampleAuthors() {

        $data = [
            'data' => [
                '11) test «Flask Web Development»',
                '12) test «Flask Web»'
            ]
        ];

        $this->post('/api/books/create/with_author', $data);

        $authors = Author::all();
        $books = Book::all();

        self::assertCount(1, $authors);
        self::assertCount(2, $books);

        self::assertEquals('test', $authors[0]->full_name);
        self::assertEquals('Flask Web Development', $books[0]->name);
        self::assertEquals('Flask Web', $books[1]->name);
    }

    public function testCreateWithAuthorSecondType() {
        $data = [
            'data' => [
                '11) test "kek"',
            ]
        ];

        $this->post('/api/books/create/with_author', $data)->json();
        $book = Book::first();
        $author = $book->authors[0];

        self::assertEquals('test', $author->full_name);
        self::assertEquals('kek', $book->name);
    }

    public function testCount() {
        factory(Book::class, 3)->create();

        $response = $this->get('/api/books/count')->json()['result'];

        self::assertEquals(3, $response);
    }

    public function testSetInPlanTrue() {
        $book = factory(Book::class)->create(['in_plan' => false]);

        $data = [
            'in_plan' => 'true',
        ];

        $this->post("/api/books/set/in_plan/{$book->id}", $data)->json();

        $new_book = Book::first();
        self::assertTrue($new_book->in_plan);
    }

    public function testSetInPlanFalse() {
        $book = factory(Book::class)->create(['in_plan' => true]);

        $data = [
            'in_plan' => 'false',
        ];

        $this->post("/api/books/set/in_plan/{$book->id}", $data)->json();

        $new_book = Book::first();
        self::assertFalse($new_book->in_plan);
    }

    public function testSetHasReadTrue() {
        $book = factory(Book::class)->create(['has_read' => false]);

        $data = [
            'has_read' => 'true',
        ];

        $this->post("/api/books/set/has_read/{$book->id}", $data)->json();

        $new_book = Book::first();
        self::assertTrue($new_book->has_read);
    }

    public function testSetHasReadFalse() {
        $book = factory(Book::class)->create(['has_read' => true]);

        $data = [
            'has_read' => 'false',
        ];

        $this->post("/api/books/set/has_read/{$book->id}", $data)->json();

        $new_book = Book::first();
        self::assertFalse($new_book->has_read);
    }

    public function testKek() {
        $book = factory(Book::class)->create();

        $author = factory(Author::class)->create();
        $second_author = factory(Author::class)->create(['deleted_at' => 'delete']);
        $third_author = factory(Author::class)->create();

        factory(AuthorBook::class)->create(['book_id' => $book->id, 'author_id' => $author->id]);
        factory(AuthorBook::class)->create(['book_id' => $book->id, 'author_id' => $second_author->id]);
        factory(AuthorBook::class)->create(['book_id' => $book->id, 'author_id' => $third_author->id, 'deleted_at' => 'delete']);

        dd($book->authors->toArray());
    }


    public function test() {

        $book = factory(Book::class)->create(['deleted_at' => 'deleted']);

        $test = Test::create([
            'book_id' => $book->id,
        ]);

        dd($test->book->toArray());
    }
}
