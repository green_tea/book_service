<?php

namespace Tests\Feature;

use App\Models\Author;
use App\Models\Book;
use Tests\TestCase;

class AuthorTest extends TestCase
{
    public function testCreate()
    {
        $data = [
            'first_name' => 'Vlad',
            'middle_name' => 'Sergeevich',
            'last_name' => 'Vetlin   ',
        ];

        $this->post('/api/authors/create', $data)->json();

        $author = Author::first();

        self::assertEquals("Vlad Sergeevich Vetlin", $author->full_name);
        self::assertEquals("Vlad", $author->first_name);
    }

    public function testGet() {
        $author = factory(Author::class)->create();

        $response = $this->get("/api/authors/{$author->id}")->json()['result'];

        self::assertEquals($author->id, $response['id']);
    }

    public function testAll() {
        $authors = factory(Author::class, 3)->create();

        $data = [
            'per_page' => 1,
            'cur_page' => 2,
        ];

        $response = $this->getQ('/api/authors', $data)->json()['result'];

        self::assertEquals(3, $response['total']);
        self::assertEquals($authors[1]->id, $response['data'][0]['id']);
        self::assertCount(1, $response['data']);
    }


}
