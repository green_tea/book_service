<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\AuthorBook;
use App\Models\Book;
use App\Services\Paginator;

class BookController extends Controller
{
    public function createWithAuthor() {
        $this->validate(request(), [
            'data' => 'array',
            'data.*' => 'string',
        ]);


        $data = array_map(function ($el) {
            return mb_substr($el, mb_strpos($el, ')') ? mb_strpos($el, ')') + 1 : 0);
        }, request('data'));


        $author_names = array_map(function ($el) {
            $index = mb_strpos($el, '«');
            if (!$index)
                $index = mb_strpos($el, '"');
            return [ 'full_name' => trim(mb_substr($el, 0, $index)) ];
        }, $data);

        $book_names = array_map(function ($el) {
            $index = mb_strpos($el, '«');
            if (!$index)
                $index = mb_strpos($el, '"');
            $book_name = mb_substr($el, $index + 1);
            return [ 'name' => trim(mb_substr($book_name, 0, -1)) ];

        }, $data);


        for ($i = 0; $i < count($author_names); ++$i) {
            $author_name = $author_names[$i];
            $book_name = $book_names[$i];

            $book = Book::where('name', $book_name['name'])->first();
            if ($book)
                continue;

            $book = Book::create($book_name);

            $author = Author::where('full_name', $author_name['full_name'])->first();
            if (!$author)
                $author = Author::create($author_name);

            AuthorBook::create([
                'author_id' => $author->id,
                'book_id' => $book->id,
            ]);

        }

        return $this->okResponse();
    }

    public function get(Book $book) {
        return $this->okResponse($book->load(['authors']));
    }

    public function all() {
        return $this->okResponse(Paginator::paginateIfNeeded(Book::query()));
    }

    public function count() {
        return $this->okResponse(Book::count());
    }

    public function setInPlan(Book $book) {
        $this->validate(request(), [
//            'in_plan' => 'string|in:true,false'
            'in_plan' => 'boolean',
        ]);

        $in_plan = request('in_plan', true);

        $book->in_plan = $in_plan;
        $book->save();

        return $this->okResponse($book->refresh());
    }

    public function setHasRead(Book $book) {

        $this->validate(request(), [
//            'has_read' => 'string|in:true,false',
            'has_read' => 'boolean',
        ]);

        $has_read = request('has_read', true);

        $book->has_read = $has_read;
        $book->in_plan = $has_read;
        $book->save();

        return $this->okResponse($book->refresh());

    }
}
