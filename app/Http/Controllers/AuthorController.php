<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Services\Paginator;

class AuthorController extends Controller
{
    public function create() {
        $rules = [
            'first_name' => 'string',
            'middle_name' => 'string',
            'last_name' => 'string',
        ];

        $this->validate(request(), $rules);

        $data = request()->only(array_keys($rules));
        $author = Author::create($data);


        $author->setFullName();
        $author->refresh();

        return $this->okResponse($author);
    }

    public function get(Author $author) {
        return $this->okResponse($author->load(['books']));
    }

    public function all() {
        return $this->okResponse(Paginator::paginateIfNeeded(Author::query()));
    }
}
