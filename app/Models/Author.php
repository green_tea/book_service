<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Author
 * @package App
 * @property $id
 * @property $full_name
 * @property $first_name
 * @property $middle_name
 * @property $last_name
 */
class Author extends Model
{
    use SoftDeletes;

    protected $fillable = [ 'full_name', 'first_name', 'middle_name', 'last_name' ];

    public function setFullName() {
        $this->full_name = trim($this->first_name . ' ' . $this->middle_name . ' ' . $this->last_name);
        $this->save();
    }

    public function books() {
        return $this->belongsToMany(Book::class, 'author_books');
    }
}

