<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Book
 * @package App
 * @property $id
 * @property $name
 * @property $has_read
 * @property $in_plan
 */
class Book extends Model
{
    protected $fillable = [ 'name', 'has_read', 'in_plan'];

    protected $casts = [
        'has_read' => 'boolean',
        'in_plan' => 'boolean',
    ];

    public function authors() {
        return $this->belongsToMany(Author::class, 'author_books');
    }
}
