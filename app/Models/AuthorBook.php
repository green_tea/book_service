<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuthorBook extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'author_id',
        'book_id',
    ];

    protected $casts = [
        'author_id' => 'number',
        'book_id' => 'number',
    ];
}
