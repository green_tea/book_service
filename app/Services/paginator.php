<?php

namespace App\Services;


use DB;

class Paginator
{
    public static function paginate($data, $per_page, $current_page)
    {
        $count = DB::table( DB::raw("({$data->toSql()}) as sub") )
            ->mergeBindings($data->getQuery())
            ->count();

        return new \Illuminate\Pagination\LengthAwarePaginator(
            $data->forPage($current_page, $per_page)->get(),
            $count,
            $per_page,
            $current_page,
            [
                "path" => "/" . request()->path() . "?" . http_build_query(request()->except("page")),
            ]
        );
    }

    public static function paginateIfNeeded($data)
    {
        $current_page = request("cur_page", 1);
        $per_page = request("per_page", 15);
        return self::paginate($data, $per_page, $current_page);
    }
}
