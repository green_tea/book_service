<?php

namespace App;

use App\Models\Book;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{

    protected $fillable = ['book_id'];

    public function book() {
        return $this->belongsTo(Book::class);
    }
}
